<?php
namespace app_bis_sekolah_admin\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_bis_sekolah_admin/assets';

    public $css = [];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'technosmart\assets_manager\VueAsset',
        'technosmart\assets_manager\VueResourceAsset',
        'technosmart\assets_manager\VueDefaultValueAsset',
        'technosmart\assets_manager\DeepstreamAsset',
        'technosmart\assets_manager\RequiredAsset',
    ];
}