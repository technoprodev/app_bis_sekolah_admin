<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "bus".
 *
 * @property integer $id
 * @property string $number_plate
 * @property double $latitude
 * @property double $longitude
 * @property string $origin
 * @property string $destination
 * @property string $created_at
 * @property string $mobile_datetime
 *
 * @property BusTracking[] $busTrackings
 * @property DriverAttendance[] $driverAttendances
 * @property StudentAttendance[] $studentAttendances
 */
class Bus extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bus';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //number_plate
            [['number_plate'], 'required'],
            [['number_plate'], 'string', 'max' => 16],
            [['number_plate'], 'unique'],

            //latitude
            [['latitude'], 'number'],

            //longitude
            [['longitude'], 'number'],

            //origin
            [['origin'], 'required'],
            [['origin'], 'string', 'max' => 32],

            //destination
            [['destination'], 'required'],
            [['destination'], 'string', 'max' => 32],

            //created_at
            [['created_at'], 'safe'],

            //mobile_datetime
            [['mobile_datetime'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number_plate' => 'Number Plate',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'origin' => 'Origin',
            'destination' => 'Destination',
            'created_at' => 'Created At',
            'mobile_datetime' => 'Mobile Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusTrackings()
    {
        return $this->hasMany(BusTracking::className(), ['id_bus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverAttendances()
    {
        return $this->hasMany(DriverAttendance::className(), ['id_bus' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAttendances()
    {
        return $this->hasMany(StudentAttendance::className(), ['id_bus' => 'id']);
    }
}
