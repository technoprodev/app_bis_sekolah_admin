<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is used as Yii::$app->user
 *
 * @property integer $id
 * @property string $username
 */
class UserIdentity extends \technosmart\models\User
{
	public function rules()
    {
        return [
            //id

            //username
            [['username'], 'trim'],
            [['username'], 'required'],
            [['username'], 'unique', 'message' => 'This username has already been taken.'],
            [['username'], 'string', 'min' => 2, 'max' => 16],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //email
            // [['email'], 'trim'],
            // [['email'], 'required'],
            [['email'], 'unique', 'message' => 'This email address has already been taken.'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],

            //auth_key
            // [['auth_key'], 'required'],
            [['auth_key'], 'string', 'max' => 32],

            //password_hash
            // [['password_hash'], 'required'],
            [['password_hash'], 'string', 'max' => 255],

            //password_reset_token
            [['password_reset_token'], 'string', 'max' => 255],
            [['password_reset_token'], 'unique'],

            // status
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE, self::STATUS_DELETED]],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //password
            ['password', 'required', 'on' => 'login'],
            ['password', 'string', 'min' => 6, 'on' => 'login'],

            ['password', 'required', 'on' => 'signup'],
            ['password', 'string', 'min' => 6, 'on' => 'signup'],

            ['password', 'required', 'on' => 'resetPassword'],
            ['password', 'string', 'min' => 6, 'on' => 'resetPassword'],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6, 'on' => 'create'],

            //repassword
            ['repassword', 'required', 'on' => 'signup'],
            ['repassword', 'string', 'min' => 6, 'on' => 'signup'],
            ['repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => 'signup'],

            ['repassword', 'required', 'on' => 'resetPassword'],
            ['repassword', 'string', 'min' => 6, 'on' => 'resetPassword'],
            ['repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => 'resetPassword'],

            ['repassword', 'required', 'on' => 'create'],
            ['repassword', 'string', 'min' => 6, 'on' => 'create'],
            ['repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => 'create'],
        ];
    }
    
    /*public function getUser()
    {
        return $this->hasOne(UserApp::className(), ['id' => 'id']);
    }*/
}