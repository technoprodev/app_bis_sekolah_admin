<?php
namespace app_bis_sekolah_admin\models;

use Yii;

class Monitoring extends \yii\base\Model
{
    public $idBus;
    public $date;

    public function __construct ()
    {
        $this->idBus = null;
        $this->date = date("d-m-Y");
    }

    public function rules()
    {
        return [
            //idBus
            [['idBus'], 'required'],

            //date
            [['date'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'idBus' => 'Bus',
            'date' => 'Date',
        ];
    }
}
