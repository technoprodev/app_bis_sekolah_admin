<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sim_number
 * @property integer $ktp_number
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $mobile_datetime
 *
 * @property DriverAttendance[] $driverAttendances
 * @property StudentAttendance[] $studentAttendances
 */
class Driver extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //sim_number
            [['sim_number'], 'required'],
            [['sim_number'], 'integer'],

            //ktp_number
            [['ktp_number'], 'required'],
            [['ktp_number'], 'integer'],

            //latitude
            [['latitude'], 'number'],

            //longitude
            [['longitude'], 'number'],

            //created_at
            [['created_at'], 'safe'],

            //mobile_datetime
            [['mobile_datetime'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sim_number' => 'Sim Number',
            'ktp_number' => 'Ktp Number',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'mobile_datetime' => 'Mobile Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverAttendances()
    {
        return $this->hasMany(DriverAttendance::className(), ['id_driver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAttendances()
    {
        return $this->hasMany(StudentAttendance::className(), ['id_driver' => 'id']);
    }
}
