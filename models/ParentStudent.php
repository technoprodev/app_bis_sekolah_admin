<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "parent_student".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property integer $hp
 *
 * @property Student[] $students
 */
class ParentStudent extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'parent_student';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //address
            [['address'], 'required'],
            [['address'], 'string'],

            //hp
            [['hp'], 'required'],
            [['hp'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'hp' => 'Hp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['id_parent' => 'id']);
    }
}
