<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $nis
 * @property string $name
 * @property string $school
 * @property string $school_type
 * @property string $address
 * @property string $created_at
 *
 * @property ParentStudent $parent
 * @property StudentAttendance[] $studentAttendances
 */
class Student extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],

            //id_parent
            [['id_parent'], 'integer'],
            [['id_parent'], 'exist', 'skipOnError' => true, 'targetClass' => ParentStudent::className(), 'targetAttribute' => ['id_parent' => 'id']],

            //nis
            [['nis'], 'required'],
            [['nis'], 'string', 'max' => 32],
            [['nis'], 'unique'],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //school
            [['school'], 'required'],
            [['school'], 'string', 'max' => 64],

            //school_type
            [['school_type'], 'required'],
            [['school_type'], 'string'],

            //address
            [['address'], 'required'],
            [['address'], 'string'],

            //created_at
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Id Parent',
            'nis' => 'Nis',
            'name' => 'Name',
            'school' => 'School',
            'school_type' => 'School Type',
            'address' => 'Address',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ParentStudent::className(), ['id' => 'id_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAttendances()
    {
        return $this->hasMany(StudentAttendance::className(), ['id_student' => 'id']);
    }
}
