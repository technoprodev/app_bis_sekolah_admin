<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "bus_tracking".
 *
 * @property integer $id
 * @property integer $id_bus
 * @property double $latitude
 * @property double $longitude
 * @property string $created_at
 * @property string $mobile_datetime
 *
 * @property Bus $bus
 */
class BusTracking extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bus_tracking';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_bus
            [['id_bus'], 'integer'],
            [['id_bus'], 'exist', 'skipOnError' => true, 'targetClass' => Bus::className(), 'targetAttribute' => ['id_bus' => 'id']],

            //latitude
            [['latitude'], 'number'],

            //longitude
            [['longitude'], 'number'],

            //created_at
            [['created_at'], 'safe'],

            //mobile_datetime
            [['mobile_datetime'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_bus' => 'Id Bus',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'mobile_datetime' => 'Mobile Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBus()
    {
        return $this->hasOne(Bus::className(), ['id' => 'id_bus']);
    }
}
