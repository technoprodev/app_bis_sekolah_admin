<?php
namespace app_bis_sekolah_admin\models;

use Yii;

/**
 * This is the model class for table "driver_attendance".
 *
 * @property integer $id
 * @property integer $id_driver
 * @property integer $id_bus
 * @property double $latitude
 * @property double $longitude
 * @property integer $status_attendance
 * @property string $created_at
 * @property string $mobile_datetime
 *
 * @property Driver $driver
 * @property Bus $bus
 */
class DriverAttendance extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'driver_attendance';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_driver
            [['id_driver'], 'required'],
            [['id_driver'], 'integer'],
            [['id_driver'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['id_driver' => 'id']],

            //id_bus
            [['id_bus'], 'integer'],
            [['id_bus'], 'exist', 'skipOnError' => true, 'targetClass' => Bus::className(), 'targetAttribute' => ['id_bus' => 'id']],

            //latitude
            [['latitude'], 'number'],

            //longitude
            [['longitude'], 'number'],

            //status_attendance
            [['status_attendance'], 'required'],
            [['status_attendance'], 'integer'],

            //created_at
            [['created_at'], 'safe'],

            //mobile_datetime
            [['mobile_datetime'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_driver' => 'Id Driver',
            'id_bus' => 'Id Bus',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'status_attendance' => 'Status Attendance',
            'created_at' => 'Created At',
            'mobile_datetime' => 'Mobile Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'id_driver']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBus()
    {
        return $this->hasOne(Bus::className(), ['id' => 'id_bus']);
    }
}
