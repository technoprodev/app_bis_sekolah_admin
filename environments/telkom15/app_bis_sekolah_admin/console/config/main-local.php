<?php
$config = [
    'components' => [
        'dba' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.60.163.17;dbname=bis-sekolah',
            'username' => 'notroot',
            'password' => 'notwebmaster',
            'charset' => 'utf8',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=10.60.163.17;dbname=bis-sekolah-technosmart',
            'username' => 'notroot',
            'password' => 'notwebmaster',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];

return $config;