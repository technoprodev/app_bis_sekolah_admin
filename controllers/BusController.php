<?php
namespace app_bis_sekolah_admin\controllers;

use Yii;
use app_bis_sekolah_admin\models\Bus;
use app_bis_sekolah_admin\models\BusTracking;
use app_bis_sekolah_admin\models\StudentAttendance;
use app_bis_sekolah_admin\models\Monitoring;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * BusController implements highly advanced CRUD actions for Bus model.
 */
class BusController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Bus'], ['create', 'Create Bus'], ['update', 'Update Bus'], ['delete', 'Delete Bus'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Bus::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('bus');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'number_plate',
                'latitude',
                'longitude',
                'origin',
                'destination',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Buses',
            ]);
        }
        
        // view single data
        $model['bus'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Bus ' . $model['bus']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['bus'] = isset($id) ? $this->findModel($id) : new Bus();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bus']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['bus'])
                );
                return $this->json($result);
            }

            $transaction['bus'] = Bus::getDb()->beginTransaction();

            try {
                if ($model['bus']->isNewRecord) {}
                if (!$model['bus']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['bus']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['bus']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['bus']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Bus',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['bus']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['bus'] = isset($id) ? $this->findModel($id) : new Bus();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bus']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['bus'])
                );
                return $this->json($result);
            }

            $transaction['bus'] = Bus::getDb()->beginTransaction();

            try {
                if ($model['bus']->isNewRecord) {}
                if (!$model['bus']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['bus']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['bus']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['bus']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Bus ' . $model['bus']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['bus']->id]);
    }

    /**
     * Deletes an existing Bus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMonitoring()
    {
        $model['buses'] = Bus::find()->all();
        // Yii::$app->d->ddx($model['buses']);

        return $this->render('monitoring', [
            'model' => $model,
            'title' => 'Monitoring All Buses Position',
        ]);
    }

    public function actionMonitoringEachBus()
    {
        $model['monitoring'] = new Monitoring();

        if($post = Yii::$app->request->post()) {
            $model['monitoring']->idBus = $post['Monitoring']['idBus'];
            $model['monitoring']->date = $post['Monitoring']['date'];
            
            $model['bus'] = Bus::find()->where(['id' => $model['monitoring']->idBus])->one();
            $model['busTrackings'] = BusTracking::find()
                ->where(['id_bus' => $model['monitoring']->idBus])
                ->andWhere(['>=', 'mobile_datetime', date("Y-m-d", strtotime($model['monitoring']->date)) . ' 00:00:00'])
                ->andWhere(['<=', 'mobile_datetime', date("Y-m-d", strtotime($model['monitoring']->date)) . ' 23:59:59'])
                ->orderBy(['mobile_datetime' => SORT_ASC])
                ->all();
            /*Yii::$app->d->ddx($model['busTrackings'],
                $model['bus'],
                date("Y-m-d", strtotime($model['monitoring']->date)),
                BusTracking::find()->where(['id_bus' => $model['monitoring']->idBus])->andWhere(['>', 'mobile_datetime', date("Y-m-d", strtotime($model['monitoring']->date)) . ' 00:00:00'])->andWhere(['<', 'mobile_datetime', date("Y-m-d", strtotime($model['monitoring']->date)) . ' 23:59:59'])->orderBy(['mobile_datetime' => SORT_ASC])->createCommand()->rawSql
            );*/

            $title = 'Monitoring Bus '. $model['bus']->number_plate . ' Position (on ' . $model['monitoring']->date . ')';
        } else {
            $title = 'Monitoring Bus';
        }

        return $this->render('monitoring-each-bus', [
            'model' => $model,
            'title' => $title,
        ]);
    }

    public function actionMonitoringEachStudent($id)
    {
        $busId = StudentAttendance::find(['id_student' => $id])->one();
        $model['busTrackings'] = BusTracking::find(['id_bus' => $busId])->orderBy(['mobile_datetime' => SORT_DESC])->one();

        return $model['busTrackings']->mobile_datetime;
        // Yii::$app->d->ddx($model['buses']);

        return $this->render('monitoring-each-student', [
            'model' => $model,
            'title' => 'Monitoring Bus ' . $model['bus']->number_plate . ' Position',
        ]);
    }
}
