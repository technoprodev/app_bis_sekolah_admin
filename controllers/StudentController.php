<?php
namespace app_bis_sekolah_admin\controllers;

use Yii;
use app_bis_sekolah_admin\models\UserIdentity;
use app_bis_sekolah_admin\models\Student;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * StudentController implements highly advanced CRUD actions for Student model.
 */
class StudentController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Student'], ['create', 'Create Student'], ['update', 'Update Student'], ['delete', 'Delete Student'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Student::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('student');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'id_parent',
                'nis',
                'name',
                'school',
                'school_type',
                'address',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Students',
            ]);
        }
        
        // view single data
        $model['user'] = $this->findModelUserIdentity($id);
        $model['student'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Student ' . $model['student']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModelUserIdentity($id) : new UserIdentity();
        $model['user']->scenario = 'repass';
        $model['student'] = isset($id) ? $this->findModel($id) : new Student();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['student']->load($post);

            if ($model['user']->isNewRecord) {
                $model['user']->status = 1;
                $model['user']->password = 'password';
                $model['user']->repassword = 'password';
            }
            $model['student']->name = $model['user']->name;

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['user']),
                    ActiveForm::validate($model['student'])
                );
                return $this->json($result);
            }

            $transaction['user'] = UserIdentity::getDb()->beginTransaction();
            $transaction['student'] = Student::getDb()->beginTransaction();

            try {
                if (!$model['user']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $model['student']->id = $model['user']->id;
                if (!$model['student']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['user']->commit();
                $transaction['student']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['user']->rollBack();
                $transaction['student']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['user']->rollBack();
                $transaction['student']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-create', [
                'model' => $model,
                'title' => 'Add New Student',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['student']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModelUserIdentity($id) : new UserIdentity();
        $model['student'] = isset($id) ? $this->findModel($id) : new Student();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['student']->load($post);

            if ($model['user']->isNewRecord) {
                $model['user']->status = 1;
                $model['user']->password = 'password';
                $model['user']->repassword = 'password';
            }
            $model['student']->name = $model['user']->name;

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['user']),
                    ActiveForm::validate($model['student'])
                );
                return $this->json($result);
            }

            $transaction['user'] = UserIdentity::getDb()->beginTransaction();
            $transaction['student'] = Student::getDb()->beginTransaction();

            try {
                if (!$model['user']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                if (!$model['student']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['user']->commit();
                $transaction['student']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['user']->rollBack();
                $transaction['student']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['user']->rollBack();
                $transaction['student']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update', [
                'model' => $model,
                'title' => 'Update Student ' . $model['student']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['student']->id]);
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the UserIdentity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserIdentity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelUserIdentity($id)
    {
        if (($model = UserIdentity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
