<?php
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app_bis_sekolah_admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_bis_sekolah_admin\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_bis_sekolah_admin',
        ],
        'user' => [
            'identityClass' => 'app_bis_sekolah_admin\models\UserIdentity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_bis_sekolah_admin', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_bis_sekolah_admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;