<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['student']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['student'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['student'], 'nis')->begin(); ?>
        <?= Html::activeLabel($model['student'], 'nis', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['student'], 'nis', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['student'], 'nis', ['class' => 'help-block']); ?>
    <?= $form->field($model['student'], 'nis')->end(); ?>

    <?= $form->field($model['user'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['user'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>

    <?= $form->field($model['student'], 'school')->begin(); ?>
        <?= Html::activeLabel($model['student'], 'school', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['student'], 'school', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['student'], 'school', ['class' => 'help-block']); ?>
    <?= $form->field($model['student'], 'school')->end(); ?>

    <?= $form->field($model['student'], 'school_type')->begin(); ?>
        <?= Html::activeLabel($model['student'], 'school_type', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['student'], 'school_type', [ 'sd' => 'SD', 'smp' => 'SMP', 'sma' => 'SMA', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['student'], 'school_type', ['class' => 'help-block']); ?>
    <?= $form->field($model['student'], 'school_type')->end(); ?>

    <?= $form->field($model['student'], 'address')->begin(); ?>
        <?= Html::activeLabel($model['student'], 'address', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['student'], 'address', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['student'], 'address', ['class' => 'help-block']); ?>
    <?= $form->field($model['student'], 'address')->end(); ?>

    <hr>

    <?= $form->field($model['user'], 'username')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'username', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['user'], 'username', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'username')->end(); ?>

<hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['student']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>