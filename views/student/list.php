<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/student/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Parent</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nis</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Name</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">School</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">School Type</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Address</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id parent" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nis" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search school" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search school type" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search address" class="form-control border-none fs-12 f-normal"/></th>
        </tr>
    </thead>
</table>