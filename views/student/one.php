<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6 margin-top-15">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->id ? $model['student']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['id_parent'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->id_parent ? $model['student']->id_parent : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['nis'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->nis ? $model['student']->nis : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['name'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->name ? $model['student']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['school'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->school ? $model['student']->school : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['school_type'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->school_type ? $model['student']->school_type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['student']->attributeLabels()['address'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['student']->address ? $model['student']->address : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['student']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['student']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>