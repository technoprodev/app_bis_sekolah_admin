<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['driver']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['driver'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['user'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>

    <?= $form->field($model['driver'], 'sim_number')->begin(); ?>
        <?= Html::activeLabel($model['driver'], 'sim_number', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['driver'], 'sim_number', ['class' => 'form-control']) ?>
        <?= Html::error($model['driver'], 'sim_number', ['class' => 'help-block']); ?>
    <?= $form->field($model['driver'], 'sim_number')->end(); ?>

    <?= $form->field($model['driver'], 'ktp_number')->begin(); ?>
        <?= Html::activeLabel($model['driver'], 'ktp_number', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['driver'], 'ktp_number', ['class' => 'form-control']) ?>
        <?= Html::error($model['driver'], 'ktp_number', ['class' => 'help-block']); ?>
    <?= $form->field($model['driver'], 'ktp_number')->end(); ?>

<hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['driver']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>