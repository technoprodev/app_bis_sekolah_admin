<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="100px;" class="">
</div>
<div class="margin-top-20 border-light border-thin shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-red text-center fs-18">
        <span class="text-lightest f-italic f-bold"><?= Yii::$app->params['app.name'] ?></span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="padding-y-15 bg-lighter text-red">
        <div class="text-center fs-16 margin-top-5">Telkom Indonesia © <?= date('Y') ?></div>
    </div>
</div>