<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);

$busTrackings = null;
if (isset($model['busTrackings']))
    foreach ($model['busTrackings'] as $key => $value) {
        $busTrackings[$key] = [
            'lat' => $model['busTrackings'][$key]->latitude,
            'lng' => $model['busTrackings'][$key]->longitude,
        ];;
    }

$this->registerJs(
    'vm.$data.busTrackings = ' . json_encode($busTrackings) . ';' .
    "
        function initMap() {
            window.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: new google.maps.LatLng(-6.1607834, 106.8124191),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        
            var busCoordinates = vm.\$data.busTrackings;
            var busPath = new google.maps.Polyline({
                path: busCoordinates,
                geodesic: true,
                strokeColor: '#2A41FF',
                strokeOpacity: 0.7,
                strokeWeight: 4
            });

            busPath.setMap(map);

            var startMarker = new google.maps.Marker({
                  position:busPath.getPath().getAt(0), 
                  map:map,
                  icon: 'https://chart.googleapis.com/chart?chst=d_map_xpin_icon&chld=pin_star|car-dealer|00FFFF|FF0000',
                });
            var endMarker =  new google.maps.Marker({
                  position:busPath.getPath().getAt(busPath.getPath().getLength()-1), 
                  map:map,
                  icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=flag|ADDE63',
                });
        }

        /*record.set('1', {id: 1, lat:-6.1607, lng:106.8124});
        record.set('20', {id: 20, lat:-6.1468, lng:106.8859});
        record.set('21', {id: 21, lat:-6.1855, lng:106.8819});
        record.set('22', {id: 22, lat:-6.1965, lng:106.8898});*/
    ",
    3
);
?>

<?php $form = ActiveForm::begin(); ?>
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <div class="box box-break-sm">
        <div class="box-2 padding-left-0">
            <?= $form->field($model['monitoring'], 'idBus')->begin(); ?>
                <?= Html::activeLabel($model['monitoring'], 'idBus', ['class' => 'control-label']); ?>
                <?= Html::activeDropDownList($model['monitoring'], 'idBus', ArrayHelper::map(\app_bis_sekolah_admin\models\Bus::find()->indexBy('id')->asArray()->all(), 'id', 'number_plate'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                <?= Html::error($model['monitoring'], 'idBus', ['class' => 'help-block']); ?>
            <?= $form->field($model['monitoring'], 'idBus')->end(); ?>
        </div>
        <div class="box-2 padding-left-0 padding-right-40">
            <?= $form->field($model['monitoring'], 'date')->begin(); ?>
                <?= Html::activeLabel($model['monitoring'], 'date', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['monitoring'], 'date', ['class' => 'form-control input-datepicker', 'maxlength' => true]); ?>
                <?= Html::error($model['monitoring'], 'date', ['class' => 'help-block']); ?>
            <?= $form->field($model['monitoring'], 'date')->end(); ?>
        </div>
        <div class="box-1 padding-left-0 padding-right-40" style="padding-top: 23px">
            <button type="submit" class="btn btn-default btn-block border-azure text-azure bg-lightest hover-bg-azure rounded-xs">Show</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php if (!isset($model['busTrackings'])): ?>

<?php elseif ($model['busTrackings']): ?>
    <div id="app">
        <div class="row">
            <div class="col-md-12">
                <div id="map" class="padding-30" style="height: 500px; height: 75vh;"></div>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="box box-break-sm">
        <div class="box-7 margin-top-30 padding-15 bg-light-red border-light-red">
            Data not found
        </div>
    </div>
<?php endif; ?>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhx0Dgxc0tv-B2Dn0oCAc4bfKV32oGDyM&callback=initMap">
</script>