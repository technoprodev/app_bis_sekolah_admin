<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['bus']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['bus'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['bus'], 'number_plate')->begin(); ?>
        <?= Html::activeLabel($model['bus'], 'number_plate', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bus'], 'number_plate', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['bus'], 'number_plate', ['class' => 'help-block']); ?>
    <?= $form->field($model['bus'], 'number_plate')->end(); ?>

    <?= $form->field($model['bus'], 'latitude')->begin(); ?>
        <?= Html::activeLabel($model['bus'], 'latitude', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bus'], 'latitude', ['class' => 'form-control']) ?>
        <?= Html::error($model['bus'], 'latitude', ['class' => 'help-block']); ?>
    <?= $form->field($model['bus'], 'latitude')->end(); ?>

    <?= $form->field($model['bus'], 'longitude')->begin(); ?>
        <?= Html::activeLabel($model['bus'], 'longitude', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bus'], 'longitude', ['class' => 'form-control']) ?>
        <?= Html::error($model['bus'], 'longitude', ['class' => 'help-block']); ?>
    <?= $form->field($model['bus'], 'longitude')->end(); ?>

    <?= $form->field($model['bus'], 'origin')->begin(); ?>
        <?= Html::activeLabel($model['bus'], 'origin', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bus'], 'origin', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['bus'], 'origin', ['class' => 'help-block']); ?>
    <?= $form->field($model['bus'], 'origin')->end(); ?>

    <?= $form->field($model['bus'], 'destination')->begin(); ?>
        <?= Html::activeLabel($model['bus'], 'destination', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bus'], 'destination', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['bus'], 'destination', ['class' => 'help-block']); ?>
    <?= $form->field($model['bus'], 'destination')->end(); ?>


<hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['bus']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>