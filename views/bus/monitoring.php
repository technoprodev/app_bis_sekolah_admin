<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$buses = null;
if (isset($model['buses']))
    foreach ($model['buses'] as $key => $value) {
        $buses[$key] = $model['buses'][$key]->attributes;
    }

$this->registerJs(
    'vm.$data.buses = ' . json_encode($buses) . ';' .
    "
        function initMap() {
            window.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: new google.maps.LatLng(-6.1607834, 106.8124191),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            window.infowindow = new google.maps.InfoWindow();
        
            var i;
            var markers = {};

            vm.\$data.buses.forEach(function(bus, index){
                bus = bus;
                var createMarker = function(id, lat, lng) {
                    //var image = 'https://image.flaticon.com/icons/png/128/130/130262.png';
                    var marker = new google.maps.Marker({
                        id: id,
                        position: new google.maps.LatLng(lat, lng),
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        //icon: image,
                        map_icon_label: '<i class=\"fa fa-bus fs-60\"></i>'
                    });
                    bus = bus;
                    google.maps.event.addListener(marker, 'click', (function(marker, index) {
                        return function() {
                            console.log(bus);
                            infowindow.setContent(bus.number_plate + ' Jurusan: ' + bus.origin + ' - ' + bus.destination);
                            infowindow.open(map, marker);
                        }
                    })(marker, index));
                    
                    markers[id] = marker;
                    return marker;
                }
                var deleteMarker = function(id) {
                    if (typeof markers[id] != 'undefined') {
                        markers[id].setMap(null);
                    }
                }

                id = bus.id;

                record.subscribe(('' + id + ''), function(a) {

                    deleteMarker(a.id);
                    createMarker(a.id, a.lat, a.lng);
                });
            });
        }

        /*record.set('1', {id: 1, lat:-6.1607, lng:106.8124});
        record.set('20', {id: 20, lat:-6.1468, lng:106.8859});
        record.set('21', {id: 21, lat:-6.1855, lng:106.8819});
        record.set('22', {id: 22, lat:-6.1965, lng:106.8898});*/
    ",
    3
);
?>

<div id="app">
    
    <div class="row">
        <div class="col-md-12">
            <div id="map" class="padding-30" style="height: 500px; height: 75vh;"></div>
        </div>
    </div>

    
</div>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhx0Dgxc0tv-B2Dn0oCAc4bfKV32oGDyM&callback=initMap">
</script>