<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/bus/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Number Plate</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Latitude</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Longitude</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Origin</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Destination</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search number plate" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search latitude" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search longitude" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search origin" class="form-control border-none fs-12 f-normal"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search destination" class="form-control border-none fs-12 f-normal"/></th>
        </tr>
    </thead>
</table>